#pragma once

#include <array>
#include <memory>
#include <vector>

#include <SDL2PP/SDL.h>

#include "game_object.h"
#include "vec.h"

class Scene
{
protected:
    SDL::Renderer &renderer;
    std::vector<std::unique_ptr<GameObject>> game_objects;
    virtual Vec2<float> get_offset() const;

public:
    explicit Scene(SDL::Renderer &renderer);
    virtual ~Scene();
    void render() const;
    virtual void simulate(const std::array<int, 128> &is_pressed);
};

class Level_1 : public Scene
{
public:
    explicit Level_1(SDL::Renderer &renderer);
    ~Level_1() override;
};
