#pragma once

#include <memory>

#include <SDL2PP/SDL.h>

#include "drawable.h"
#include "vec.h"

class GameObject : public Drawable
{
public:
    explicit GameObject(SDL::Renderer &renderer);
    ~GameObject() override;
    virtual void control(const std::array<int, 128> &is_pressed) = 0;
};
