#include "scene.h"

#include <algorithm>

#include "character.h"

Scene::Scene(SDL::Renderer &renderer)
    : renderer{renderer}
{
}

Scene::~Scene() = default;

Vec2<float> Scene::get_offset() const
{
    // TODO: Change this based on the camera.
    return Vec2<float>(0, 0);
}

void Scene::render() const
{
    renderer.clear();
    const Vec2<float> offset = get_offset();
    for (const auto &game_object : game_objects)
    {
        game_object->draw(offset);
    }
    renderer.present();
}

void Scene::simulate(const std::array<int, 128> &is_pressed)
{
    for (const auto &game_object : game_objects)
    {
        game_object->control(is_pressed);
    }
}

Level_1::Level_1(SDL::Renderer &renderer)
    : Scene{renderer}
{
    auto skeleton = std::make_unique<Character>(renderer);
    skeleton->set_entry(LpcEntry::Type::BODY, std::vector<std::string>{"skeleton"});
    game_objects.push_back(std::move(skeleton));
}

Level_1::~Level_1() = default;
