#include <SDL2PP/SDL.h>

#include "scene.h"

#define FPS 8

const SDL::Timer::Callback callback = [](Uint32 ms, void *param)
{
    SDL::Event ev{};
    ev.set_type(SDL::Event::Type::USEREVENT);
    ev.push();
    return ms;
};

void run(void)
{
    auto window = SDL::Window{"Labyrinth", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 2 * Drawable::GRID_SIZE, 2 * Drawable::GRID_SIZE, 0};
    auto renderer = SDL::Renderer{window, -1, SDL::Renderer::Flags::ACCELERATED};
    renderer.clear();

    std::array<int, 128> is_pressed{};

    SDL::Timer timer{1000 / FPS, callback, nullptr};

    const std::unique_ptr<Scene> current_scene = std::make_unique<Level_1>(renderer);

    bool quit = false;
    while (!quit)
    {
        SDL::Event event{};
        event.wait();
        switch (event.get_type())
        {
        case SDL::Event::Type::USEREVENT:
            current_scene->simulate(is_pressed);
            current_scene->render();
            break;
        case SDL::Event::Type::KEYDOWN:
            if (auto keycode = static_cast<SDL_Event>(event).key.keysym.sym; keycode < 128)
            {
                is_pressed[keycode] = true;
            }
            break;
        case SDL::Event::Type::KEYUP:
            if (auto keycode = static_cast<SDL_Event>(event).key.keysym.sym; keycode < 128)
            {
                is_pressed[keycode] = false;
            }
            break;
        case SDL::Event::Type::QUIT:
            quit = true;
            break;
        default:
            break;
        }
    }
}

int main(void)
{
    SDL::init(SDL::InitFlags::EVERYTHING);
    run();
    SDL::quit();
    return 0;
}
