#include "drawable.h"

const int Drawable::GRID_SIZE = 256;

Drawable::Drawable(SDL::Renderer &renderer)
    : _renderer{renderer}
{
}

Drawable::~Drawable() = default;
