#include "action.h"

Action::Action(Type type)
    : _type{type}
{
}

Action::Type Action::get_type() const
{
    return _type;
}

int Action::get_frame() const
{
    return _frame;
}

void Action::next_frame()
{
    ++_frame;
    switch (_type)
    {
    case Type::BOW:
        if (_frame > 13)
        {
            _frame = 5;
        }
        break;
    case Type::HURT:
        if (_frame > 6)
        {
            _frame = 1;
        }
        break;
    case Type::SLASH:
        if (_frame > 6)
        {
            _frame = 1;
        }
        break;
    case Type::SPELLCAST:
        if (_frame > 7)
        {
            _frame = 1;
        }
        break;
    case Type::THRUST:
        if (_frame > 8)
        {
            _frame = 5;
        }
        break;
    case Type::WALKCYCLE:
        if (_frame > 9)
        {
            _frame = 2;
        }
        break;
    default:
        break;
    }
}
