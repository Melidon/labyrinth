#pragma once

template <class T = float>
class Vec2
{
public:
    T x;
    T y;
    explicit constexpr Vec2(T x, T y)
        : x{x}, y{y}
    {
    }
    Vec2 operator+(const Vec2 &other) const
    {
        return Vec2{x + other.x, y + other.y};
    }
    Vec2 operator+=(const Vec2 &other)
    {
        return *this = *this + other;
    }
    Vec2 operator*(const Vec2 &other) const
    {
        return Vec2{x * other.x, y * other.y};
    }
};
