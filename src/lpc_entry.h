#pragma once

#include <array>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

#include "action.h"
#include "direction.h"
#include "drawable.h"

class LpcEntry : public Drawable
{
public:
    enum class Type : int
    {
        NULL_OBJECT = 0,
        WEAPON = 1,
        HANDS = 2,
        HEAD = 3,
        BELT = 4,
        TORSO = 5,
        LEGS = 6,
        FEET = 7,
        BODY = 8,
        BEHIND = 9
    };
    static void clear_texture_cache(void);
    using texture_cache_key_t = std::tuple<SDL::Renderer &, const std::string &, Action::Type>;

private:
    static const int LPC_ENRTY_SIZE;
    static const std::unordered_map<Type, std::string> prefixes;
    static const std::unordered_map<Action::Type, std::string> folders;
    static std::unordered_map<texture_cache_key_t, SDL::Texture> texture_cache;
    static SDL::Texture &get_texture(SDL::Renderer &renderer, const std::string &image_name, Action::Type action_type);
    static SDL::Rect get_srcrect(Direction direction, const Action &action);
    static std::vector<std::string> transform_image_names(Type type, const std::vector<std::string> &image_names);

public:
    LpcEntry(SDL::Renderer &renderer, Type type = Type::NULL_OBJECT, const std::vector<std::string> &image_names = std::vector<std::string>{});
    ~LpcEntry() override;
    void draw(Vec2<float> offset) const override;
    void draw(Vec2<float> offset, Direction direction, const Action &action) const;

    Type get_type(void) const;

private:
    Type _type;
    std::vector<std::string> _image_names;
};
