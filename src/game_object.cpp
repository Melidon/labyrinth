#include "game_object.h"

GameObject::GameObject(SDL::Renderer &renderer)
    : Drawable{renderer}
{
}

GameObject::~GameObject() = default;
