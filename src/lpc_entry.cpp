#include "lpc_entry.h"

#include <ranges>

const int LpcEntry::LPC_ENRTY_SIZE = 64;

const std::unordered_map<LpcEntry::Type, std::string> LpcEntry::prefixes = {
    {Type::WEAPON, "WEAPON_"},
    {Type::HANDS, "HANDS_"},
    {Type::HEAD, "HEAD_"},
    {Type::BELT, "BELT_"},
    {Type::TORSO, "TORSO_"},
    {Type::LEGS, "LEGS_"},
    {Type::FEET, "FEET_"},
    {Type::BODY, "BODY_"},
    {Type::BEHIND, "BEHIND_"},
};

const std::unordered_map<Action::Type, std::string> LpcEntry::folders = {
    {Action::Type::BOW, "./lpc_entry/png/bow/"},
    {Action::Type::HURT, "./lpc_entry/png/hurt/"},
    {Action::Type::SLASH, "./lpc_entry/png/slash/"},
    {Action::Type::SPELLCAST, "./lpc_entry/png/spellcast/"},
    {Action::Type::THRUST, "./lpc_entry/png/thrust/"},
    {Action::Type::WALKCYCLE, "./lpc_entry/png/walkcycle/"},
};

template <>
struct std::hash<LpcEntry::texture_cache_key_t>
{
    std::size_t operator()(LpcEntry::texture_cache_key_t const &key) const noexcept
    {
        const auto &[renderer, image_name, action_type] = key;
        const std::size_t h1 = std::hash<SDL::Renderer *>{}(&renderer);
        const std::size_t h2 = std::hash<std::string>{}(image_name);
        const std::size_t h3 = std::hash<int>{}(static_cast<int>(action_type));
        const std::size_t c1 = h1 ^ (h2 << 1);
        const std::size_t c2 = c1 ^ (h3 << 1);
        return c2;
    }
};

std::unordered_map<LpcEntry::texture_cache_key_t, SDL::Texture> LpcEntry::texture_cache{};

SDL::Texture &LpcEntry::get_texture(SDL::Renderer &renderer, const std::string &image_name, Action::Type action_type)
{
    const auto key = texture_cache_key_t{renderer, image_name, action_type};
    if (const auto iterator = texture_cache.find(key); iterator != texture_cache.end())
    {
        return iterator->second;
    }
    else
    {
        auto texture = SDL::Texture{renderer, folders.at(action_type) + image_name};
        const auto [node, success] = texture_cache.try_emplace(key, std::move(texture));
        return node->second;
    }
}

void LpcEntry::clear_texture_cache(void)
{
    texture_cache.clear();
}

SDL::Rect LpcEntry::get_srcrect(Direction direction, const Action &action)
{
    if (action.get_type() == Action::Type::HURT)
    {
        direction = static_cast<Direction>(0);
    }
    return SDL::Rect{LPC_ENRTY_SIZE * (action.get_frame() - 1), LPC_ENRTY_SIZE * static_cast<int>(direction), LPC_ENRTY_SIZE, LPC_ENRTY_SIZE};
}

std::vector<std::string> LpcEntry::transform_image_names(Type type, const std::vector<std::string> &image_names)
{
    const auto lambda = [type](const std::string &image_name) -> std::string
    {
        return prefixes.at(type) + image_name + ".png";
    };
    const auto transformed = image_names | std::views::transform(lambda);
    return std::vector(transformed.begin(), transformed.end());
};

LpcEntry::LpcEntry(SDL::Renderer &renderer, Type type, const std::vector<std::string> &image_names)
    : Drawable{renderer},
      _type{type},
      _image_names{transform_image_names(type, image_names)}
{
    if (type == Type::NULL_OBJECT && !this->_image_names.empty())
    {
        throw std::invalid_argument{std::string{"If you are using Type::NULL_OBJECT, you must pass an empty vector for the image_names!"}};
    }
    if (type != Type::NULL_OBJECT && this->_image_names.empty())
    {
        throw std::invalid_argument{std::string{"If you are not using Type::NULL_OBJECT, you must not pass an empty vector for the image_names!"}};
    }
}

LpcEntry::~LpcEntry() = default;

void LpcEntry::draw(Vec2<float> offset) const
{
    draw(offset, static_cast<Direction>(0), Action{Action::Type::WALKCYCLE});
}

void LpcEntry::draw(Vec2<float> offset, Direction direction, const Action &action) const
{
    const SDL::Rect srcrect = get_srcrect(direction, action);
    const SDL::Rect dstrect{static_cast<int>(offset.x * GRID_SIZE), static_cast<int>(offset.y * GRID_SIZE), GRID_SIZE, GRID_SIZE};
    for (const auto &image_name : _image_names)
    {
        auto &texture = get_texture(_renderer, image_name, action.get_type());
        _renderer.copy(texture, srcrect, dstrect);
    }
}

LpcEntry::Type LpcEntry::get_type(void) const
{
    return this->_type;
}
