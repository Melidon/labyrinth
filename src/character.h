#pragma once

#include <memory>

#include "action.h"
#include "direction.h"
#include "game_object.h"
#include "lpc_entry.h"
#include "vec.h"

class Character : public GameObject
{
private:
    Vec2<float> _position;
    Direction _direction = static_cast<Direction>(0);
    Action _action{Action::Type::WALKCYCLE};
    std::unordered_map<LpcEntry::Type, std::unique_ptr<LpcEntry>> _entries{};
    void walk(Direction direction);

public:
    explicit Character(SDL::Renderer &renderer, Vec2<float> position = Vec2{0.0f, 0.0f});
    ~Character() override;
    void draw(Vec2<float> offset) const override;
    void control(const std::array<int, 128> &is_pressed) override;

    void set_entry(LpcEntry::Type type, const std::vector<std::string> &image_names);
};
