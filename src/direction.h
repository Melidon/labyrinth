#pragma once

enum class Direction : int
{
    UP = 0,
    LEFT = 1,
    DOWN = 2,
    RIGHT = 3
};
