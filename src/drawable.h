#pragma once

#include <memory>

#include <SDL2PP/SDL.h>

#include "vec.h"

class Drawable
{
public:
    static const int GRID_SIZE;

protected:
    SDL::Renderer &_renderer;

public:
    explicit Drawable(SDL::Renderer &renderer);
    virtual ~Drawable();

    virtual void draw(Vec2<float> offset) const = 0;
};
