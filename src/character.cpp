#include "character.h"

Character::Character(SDL::Renderer &renderer, Vec2<float> position)
    : GameObject{renderer},
      _position{position}
{
}

Character::~Character() = default;

void Character::draw(Vec2<float> offset) const
{
    static constexpr std::array<LpcEntry::Type, 9> entry_types = {
        LpcEntry::Type::BODY,
        LpcEntry::Type::FEET,
        LpcEntry::Type::LEGS,
        LpcEntry::Type::TORSO,
        LpcEntry::Type::BELT,
        LpcEntry::Type::HEAD,
        LpcEntry::Type::HANDS,
        LpcEntry::Type::WEAPON,
        LpcEntry::Type::BEHIND};
    for (const auto entry_type : entry_types)
    {
        if (const auto iterator = _entries.find(entry_type); iterator != _entries.end())
        {
            iterator->second->draw(offset + _position, _direction, _action);
        }
    }
}

void Character::control(const std::array<int, 128> &is_pressed)
{
    bool vertical_move = is_pressed['w'] != is_pressed['s'];
    bool horizontal_move = is_pressed['a'] != is_pressed['d'];

    if (vertical_move)
    {
        if (is_pressed['w'])
        {
            walk(Direction::UP);
        }
        if (is_pressed['s'])
        {
            walk(Direction::DOWN);
        }
        return;
    }

    if (horizontal_move)
    {
        if (is_pressed['a'])
        {
            walk(Direction::LEFT);
        }
        if (is_pressed['d'])
        {
            walk(Direction::RIGHT);
        }
        return;
    }

    // Idle
    _action = Action{Action::Type::WALKCYCLE};
}

void Character::walk(Direction direction)
{
    _direction = direction;
    if (_action.get_type() == Action::Type::WALKCYCLE)
    {
        _action.next_frame();
    }
    else
    {
        _action = Action{Action::Type::WALKCYCLE};
    }
    static constexpr std::array<Vec2<float>, 4> direction_vectors{Vec2{0.0f, -1.0f}, Vec2{-1.0f, 0.0f}, Vec2{0.0f, 1.0f}, Vec2{1.0f, 0.0f}};
    Vec2 speed{0.06f, 0.06f};
    _position += direction_vectors[static_cast<int>(direction)] * speed;
}

void Character::set_entry(LpcEntry::Type type, const std::vector<std::string> &image_names)
{
    if (image_names.empty())
    {
        throw std::invalid_argument{std::string{"At least one texture name must be specified!"}};
    }
    this->_entries[type] = std::make_unique<LpcEntry>(_renderer, type, image_names);
}
