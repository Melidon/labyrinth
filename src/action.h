#pragma once

class Action
{
public:
    enum class Type : int
    {
        BOW = 0,
        HURT = 1,
        SLASH = 2,
        SPELLCAST = 3,
        THRUST = 4,
        WALKCYCLE = 5
    };

private:
    int _frame = 1;
    Type _type;

public:
    explicit Action(Type type);
    Action::Type get_type() const;
    int get_frame() const;
    void next_frame();
};
