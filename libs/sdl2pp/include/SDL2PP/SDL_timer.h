#pragma once

#include <SDL2/SDL_timer.h>

namespace SDL
{
    class Timer
    {
    private:
        SDL_TimerID timer_id;

    public:
        using Callback = SDL_TimerCallback;

        explicit Timer(Uint32 interval, Callback callback, void *param);
        virtual ~Timer();
        Timer(const Timer &other) = delete;
        Timer(Timer &&other) noexcept;
        Timer &operator=(const Timer &other) = delete;
        Timer &operator=(Timer &&other) noexcept;

        bool operator==(const Timer &other) const;
        bool operator!=(const Timer &other) const;

        operator SDL_TimerID &();

        bool remove();
    };
}
