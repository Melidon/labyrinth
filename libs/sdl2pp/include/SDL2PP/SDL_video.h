#pragma once

#include <SDL2/SDL_video.h>
#include <string>

namespace SDL
{
    class Window
    {
    private:
        SDL_Window *wrapped;

    public:
        explicit Window(std::string const &title, int x, int y, int w, int h, Uint32 flags);
        virtual ~Window();
        Window(const Window &other) = delete;
        Window(Window &&other) noexcept;
        Window &operator=(const Window &other) = delete;
        Window &operator=(Window &&other) noexcept;

        bool operator==(const Window &other) const;
        bool operator!=(const Window &other) const;

        SDL_Window *get_wrapped(void);
    };
}
