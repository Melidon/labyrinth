#include "SDL2PP/SDL_video.h"
#include <stdexcept>

namespace SDL
{
    Window::Window(std::string const &title, int x, int y, int w, int h, Uint32 flags)
        : wrapped{SDL_CreateWindow(title.c_str(), x, y, w, h, flags)}
    {
        if (wrapped == nullptr)
        {
            throw std::runtime_error{std::string{SDL_GetError()}};
        }
    }

    Window::~Window()
    {
        SDL_DestroyWindow(wrapped);
    }

    Window::Window(Window &&other) noexcept
        : wrapped{other.wrapped}
    {
        other.wrapped = nullptr;
    }

    Window &Window::operator=(Window &&other) noexcept
    {
        if (this != &other)
        {
            SDL_DestroyWindow(wrapped);
            wrapped = other.wrapped;
            other.wrapped = nullptr;
        }
        return *this;
    }

    bool Window::operator==(const Window &other) const = default;

    bool Window::operator!=(const Window &other) const = default;

    SDL_Window *Window::get_wrapped(void)
    {
        return wrapped;
    }
}
