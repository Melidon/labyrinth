#include "SDL2PP/SDL.h"
#include <stdexcept>
#include <string>

namespace SDL
{
    void init(InitFlags flags)
    {
        if (SDL_Init(static_cast<Uint32>(flags)) < 0)
        {
            throw std::runtime_error{std::string{SDL_GetError()}};
        }
    }

    void quit(void)
    {
        SDL_Quit();
    }
}
