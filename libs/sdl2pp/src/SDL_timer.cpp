#include "SDL2PP/SDL_timer.h"
#include <stdexcept>

namespace SDL
{
    Timer::Timer(Uint32 interval, Callback callback, void *param)
        : timer_id{SDL_AddTimer(interval, callback, param)}
    {
        if (timer_id == 0)
        {
            throw std::runtime_error{std::string{SDL_GetError()}};
        }
    }

    Timer::~Timer()
    {
        remove();
    }

    Timer::Timer(Timer &&other) noexcept
        : timer_id{other.timer_id}
    {
        other.timer_id = 0;
    }

    Timer &Timer::operator=(Timer &&other) noexcept
    {
        if (this != &other)
        {
            remove();
            timer_id = other.timer_id;
            other.timer_id = 0;
        }
        return *this;
    }

    bool Timer::operator==(const Timer &other) const = default;

    bool Timer::operator!=(const Timer &other) const = default;

    Timer::operator SDL_TimerID &()
    {
        return timer_id;
    }

    bool Timer::remove()
    {
        return SDL_RemoveTimer(timer_id) == SDL_TRUE;
    }
}
