#include "SDL2PP/SDL_render.h"
#include <stdexcept>

namespace SDL
{
    Renderer::Renderer(Window &window, int index, Flags flags)
        : wrapped{SDL_CreateRenderer(window.get_wrapped(), index, static_cast<Uint32>(flags))}
    {
        if (wrapped == nullptr)
        {
            throw std::runtime_error{std::string{SDL_GetError()}};
        }
    }

    Renderer::~Renderer()
    {
        SDL_DestroyRenderer(wrapped);
    }

    Renderer::Renderer(Renderer &&other) noexcept
        : wrapped{other.wrapped}
    {
        other.wrapped = nullptr;
    }

    Renderer &Renderer::operator=(Renderer &&other) noexcept
    {
        if (this != &other)
        {
            SDL_DestroyRenderer(wrapped);
            wrapped = other.wrapped;
            other.wrapped = nullptr;
        }
        return *this;
    }

    bool Renderer::operator==(const Renderer &other) const = default;

    bool Renderer::operator!=(const Renderer &other) const = default;

    SDL_Renderer *Renderer::get_wrapped(void)
    {
        return wrapped;
    }

    void Renderer::clear(void)
    {
        if (SDL_RenderClear(wrapped) < 0)
        {
            throw std::runtime_error{std::string{SDL_GetError()}};
        }
    }

    void Renderer::copy(Texture &texture, const Rect &srcrect, const Rect &dstrect)
    {
        if (SDL_RenderCopy(wrapped, texture.get_wrapped(), &srcrect, &dstrect) < 0)
        {
            throw std::runtime_error{std::string{SDL_GetError()}};
        }
    }

    void Renderer::present(void)
    {
        SDL_RenderPresent(wrapped);
    }

    Texture::Texture(Renderer &renderer, Uint32 format, int access, int w, int h)
        : wrapped{SDL_CreateTexture(renderer.get_wrapped(), format, access, w, h)}
    {
        if (wrapped == nullptr)
        {
            throw std::runtime_error{std::string{SDL_GetError()}};
        }
    }

    Texture::~Texture()
    {
        SDL_DestroyTexture(wrapped);
    }

    Texture::Texture(Texture &&other) noexcept
        : wrapped{other.wrapped}
    {
        other.wrapped = nullptr;
    }

    Texture &Texture::operator=(Texture &&other) noexcept
    {
        if (this != &other)
        {
            SDL_DestroyTexture(wrapped);
            wrapped = other.wrapped;
            other.wrapped = nullptr;
        }
        return *this;
    }

    bool Texture::operator==(const Texture &other) const = default;

    bool Texture::operator!=(const Texture &other) const = default;

    SDL_Texture *Texture::get_wrapped(void)
    {
        return wrapped;
    }
}
