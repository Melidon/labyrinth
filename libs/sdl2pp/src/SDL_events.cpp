#include "SDL2PP/SDL_events.h"
#include <stdexcept>

namespace SDL
{
    Event::Event(void)
    {
    }

    Event::~Event()
    {
    }

    Event::operator SDL_Event &()
    {
        return wrapped;
    }

    bool Event::wait(void)
    {
        int retval = SDL_WaitEvent(&wrapped);
        switch (retval)
        {
        case 1:
            return true;
        default:
            throw std::runtime_error{std::string{SDL_GetError()}};
        }
    }

    bool Event::push(void)
    {
        int retval = SDL_PushEvent(&wrapped);
        switch (retval)
        {
        case 1:
            return true;
        case 0:
            return false;
        default:
            throw std::runtime_error{std::string{SDL_GetError()}};
        }
    }

    Event::Type Event::get_type(void) const
    {
        return static_cast<Event::Type>(wrapped.type);
    }

    void Event::set_type(Event::Type type)
    {
        wrapped.type = static_cast<Uint32>(type);
    }
}
